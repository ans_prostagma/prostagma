import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {ChatService} from "../services/chat.service";

@Component({
  selector: 'app-auth-login',
  templateUrl: './auth-login.component.html',
  styleUrls: ['./auth-login.component.scss']
})
export class AuthLoginComponent implements OnInit {

  constructor(public authService: AuthService, private chatService: ChatService) {
  }

  public message = '';
  authLoginForm = new FormGroup({
    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });

  authSubscribeForm = new FormGroup({
    email: new FormControl(
      '', Validators.email),
    password: new FormControl(
      '',
      Validators.pattern(/^(?=.{8,24}$)(?=.*[A-Z]{+})(?=.*[a-z]{+})(?=.*\d+,})(?=.*[\x21-\x2f]{+}).*$/)),
    username: new FormControl(
      '',
      [Validators.required,
        Validators.pattern(/^([a-zA-Z\d \xC0-\xF6\-œ\xF8-\xFF]{+})$/)])
  });

  get email() {
    return this.authSubscribeForm.get('email');
  }

  get password() {
    return this.authSubscribeForm.get('password');
  }

  get username() {
    return this.authSubscribeForm.get('username');
  }

  hover(authChild) {
    console.log(authChild);
    authChild.style.boxShadow = '0, 0, 5px, 1';
  }

  onSubmitSignUp() {
    this.authService.saveUser(this.authSubscribeForm.value).subscribe(res => {
      if (!res.success) {
        this.message = res.message;
      }
      this.chatService.pingConnection(res.object);
    });
  }

  onSubmit(id: any) {
    console.log(id);
    this.authService.log(this.authLoginForm.value).subscribe(res => {
      if (!res.success) {
        this.message = res.message;
      }
      this.chatService.pingConnection(res.object);
      this.authService._setSession(res);

    });
  }
  ngOnInit() {
  }

}
