import {Component, OnInit} from '@angular/core';
import {ChatService} from './services/chat.service';
import {AuthService} from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public counter = 0;
  public notification = '';
  title = 'prostagma';

  constructor(public authService: AuthService) {
  }

  ngOnInit() {
  }
}
